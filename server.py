from json import loads
import sys

import aiohttp
from aiohttp import web, WSMsgType


async def websocket_handler(request):
    ws = web.WebSocketResponse()

    await ws.prepare(request)

    init_message = await ws.receive_json()

    print(init_message)

    try:
        nickname = init_message["name"]
    except KeyError:
        sys.exit("Invalid introduction payload")

    print(f"Registering '{nickname}'")

    request.app["users"][nickname] = ws

    print(request.app["users"])

    async for msg in ws:
        if msg.type == WSMsgType.TEXT:
            if msg.data == "close":
                await ws.close()
                break

            data = loads(msg.data)

            if data["type"] == "request":
                if data["to"] in request.app["users"]:
                    other_ws = request.app["users"][data["to"]]
                    await other_ws.send_json(data)
                else:
                    await ws.send_json({"type": "error", "message": "No such recipient!"})
            elif data["type"] in ["request-accept", "request-deny", "rtc-offer", "rtc-answer", "rtc-ice-candidate"]:
                other_ws = request.app["users"][data["to"]]
                await other_ws.send_json(data)
            elif data["type"] == "message":
                if data["to"] in request.app["users"]:
                    other_ws = request.app["users"][data["to"]]
                    await other_ws.send_json(data)
                else:
                    await ws.send_json({"type": "error", "message": "No such recipient!"})
            else:
                await ws.send_str(msg.data + "/answer")
        elif msg.type == WSMsgType.ERROR:
            print("ws connection closed with exception: %s" %
                  ws.exception())

    del request.app["users"][nickname]

    print(f"Removed '{nickname}'")
    print(request.app["users"])

    return ws

app = web.Application()

app.add_routes([
    web.get('/ws', websocket_handler),
    web.static("/", "static/"),
])

app["users"] = {}

web.run_app(app)