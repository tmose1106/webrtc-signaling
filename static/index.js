const connectButton = document.getElementById('connect'),
      disconnectButton = document.getElementById('disconnect'),
      nameInput = document.getElementById('name');

const activateConnectSection = (bool) => {
    disconnectButton.disabled = bool;

    for (const element of [connectButton, nameInput]) {
        element.disabled = !bool;
    }
};

activateConnectSection(true);

const otherNameInput = document.getElementById('other-name'),
      sendButton = document.getElementById('send'),
      responseContainer = document.getElementById('response');

const activateRequestSection = (bool) => {
    for (const element of [otherNameInput, sendButton]) {
        element.disabled = !bool;
    }
};

activateRequestSection(false);

const messageSendTextarea = document.getElementById('message-body'),
      messageSendButton = document.getElementById('message-send'),
      messageRecieveBox = document.getElementById('message-box');

const activateSendSection = (bool) => {
    for (const element of [messageSendTextarea, messageSendButton]) {
        element.disabled = !bool;
    }
};

activateSendSection(false);

const showMessage = (name, message) => {
    const messageElement = document.createElement('p');

    const nameText = document.createElement('b');

    nameText.appendChild(document.createTextNode(name));

    messageElement.appendChild(nameText);
    messageElement.appendChild(document.createTextNode(message));

    responseContainer.appendChild(messageElement);
};

const showPeerMessage = (name, message) => {
    const messageElement = document.createElement('p');

    const nameText = document.createElement('b');

    nameText.appendChild(document.createTextNode(name));

    messageElement.appendChild(nameText);
    messageElement.appendChild(document.createTextNode(message));

    messageRecieveBox.appendChild(messageElement);
};

let nickname;

const servers = {
    iceServers: [
        { urls: 'stun:stun.services.mozilla.org' },
    ]
};

let rtcConnection;
let rtcChannel;

const socket = new WebSocket('ws://localhost:8080/ws');

// Connection opened
socket.addEventListener('open', (event) => {
    connectButton.disabled = false;
});

// Listen for messages
socket.addEventListener('message', (event) => {
    const data = JSON.parse(event.data);

    switch (data['type']) {
    case 'message':
        showMessage(data['from'], data['body']);
        break;
    case 'request':
        if (confirm(`User ${data['from']} has challenged you!`)) {
            showMessage('CLIENT', `You accepted challenge from '${data['from']}'`);

            rtcConnection = new RTCPeerConnection(servers);

            rtcConnection.onicecandidate =  (event) => {
                console.log('candidate event', event);

                if (!event.candidate) {
                    return;
                }

                const candiateString = JSON.stringify(event.candidate);

                console.log('candidate string', candiateString);

                socket.send(JSON.stringify({
                    type: 'rtc-ice-candidate',
                    to: data['from'],
                    from: nickname,
                    squid: true,
                    candidate: candiateString,
                }));
            };

            rtcConnection.ondatachannel = (event) => {
                console.log('new channel', event);

                rtcChannel = event.channel;

                rtcChannel.onopen = (event) => console.log('channel opened', event);
                rtcChannel.onclose = (event) => console.log('channel closed', event);
                rtcChannel.onmessage = (event) => {
                    console.log('recieved message', event);

                    showPeerMessage(`${data['from']}:`, event.data);
                };
            };

            activateRequestSection(false);

            activateSendSection(true);

            socket.send(JSON.stringify({
                type: 'request-accept',
                to: data['from'],
                from: nickname,
            }));
        } else {
            socket.send(JSON.stringify({
                type: 'request-decline',
                to: data['from'],
                from: nickname,
            }));

            showMessage('CLIENT', `You declined challenge from '${data['from']}'`);
        }

        break;
    case 'request-accept':
        showMessage('SERVER', `Challenge accepted by '${data['from']}'`);

        activateRequestSection(false);

        activateSendSection(true);

        rtcConnection = new RTCPeerConnection(servers);

        rtcConnection.onicecandidate = (event) => console.log('accept', event);

        rtcChannel = rtcConnection.createDataChannel('sendDataChannel');

        rtcChannel.onopen = (event) => console.log('channel opened', event);
        rtcChannel.onclose = (event) => console.log('channel closed', event);
        rtcChannel.onmessage = (event) => {
            console.log('recieved message', event);

            showPeerMessage(data['from'], event.data);
        };

        rtcConnection.createOffer(
            (desc) => {
                console.log('new offer:', desc);

                rtcConnection.setLocalDescription(desc);

                socket.send(JSON.stringify({
                    type: 'rtc-offer',
                    to: data['from'],
                    from: nickname,
                    sdp: JSON.stringify(desc),
                }));
            } ,
            (error) => console.log('error occured during offer:', error.toString())
        );

        // rtcChannel.onIceCanidate = (event) => {

        // };

        break;
    case 'request-decline':
        showMessage('SERVER', `Challenge denied by '${data['from']}'`);

        break;
    case 'rtc-offer':
        const sdpOffer = JSON.parse(data['sdp']);

        console.log('remote offer:', sdpOffer);

        rtcConnection.setRemoteDescription(sdpOffer);

        rtcConnection.createAnswer().then(
            (desc) => {
                rtcConnection.setLocalDescription(desc);

                socket.send(JSON.stringify({
                    type: 'rtc-answer',
                    to: data['from'],
                    from: nickname,
                    sdp: JSON.stringify(desc),
                }));
            },
            (error) => console.log('error occured during answer:', error.toString())
        );

        break;
    case 'rtc-answer':
        rtcConnection.setRemoteDescription(JSON.parse(data['sdp']));

        break;
    case 'rtc-ice-candidate':
            console.log('recieved data:', data);
            console.log('recieved candidate:', data['candidate']);

            const candidateInfo = JSON.parse(data['candidate']),
            candidate = new RTCIceCandidate(candidateInfo);

            rtcConnection.addIceCandidate(candidate).then(
                console.log('Added canidate:', candidate),
                (error) => console.log('Failed to add canidate:', error.toString())
            );

        break;
    case 'error':
        showMessage('SERVER', data['message']);
        break;
    default:
        console.log(`No handler for '${data['type']}' type!`);
        break;
    }
});

connectButton.addEventListener('click', () => {
    const payload = {
        type: 'connect',
        name: nameInput.value,
    };

    console.log(`Connecting as '${nameInput.value}'`);

    socket.send(JSON.stringify(payload));

    activateConnectSection(false);

    activateRequestSection(true);

    nickname = nameInput.value;
});

sendButton.addEventListener('click', () => {
    const payload = {
        type: 'request',
        to: otherNameInput.value,
        from: nickname,
    }

    socket.send(JSON.stringify(payload));
});

messageSendButton.addEventListener('click', () => {
    const message = messageSendTextarea.value;

    rtcChannel.send(message);

    showPeerMessage('You:', message);
})
